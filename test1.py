import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# 定义数据点
x_data = np.array([0, 0.454681, 0.784881, 1.00877, 1.42957, 2.00773, 2.70301, 3.5026, 4.28938, 5.10089, 5.99042])
y_data = np.array([0, 0.0644131, 0.0880665, 0.100409, 0.120022, 0.144428, 0.167301, 0.189952, 0.213912, 0.230126, 0.244892])

# 定义幂函数模型
def power_func(x, a, b):
    return a * np.power(x, b)

# 移除 x=0 的点，因为幂函数在 x=0 时可能无定义
x_data_fit = x_data[1:]
y_data_fit = y_data[1:]

# 使用 curve_fit 进行拟合
params, covariance = curve_fit(power_func, x_data_fit, y_data_fit, p0=[1, 1])

# 获取拟合参数
a, b = params

# 生成拟合曲线
x_fit = np.linspace(0.1, 6, 100)  # 从 0.1 开始，避免 x=0
y_fit = power_func(x_fit, a, b)

# 绘制图像
plt.figure(figsize=(10, 6))
plt.scatter(x_data, y_data, label='Data Points', color='red')
plt.plot(x_fit, y_fit, label=f'Fitted Power Function: y = {a:.4f} * x^{b:.4f}', color='blue')
plt.xlabel('x')
plt.ylabel('y')
plt.title('Power Function Fitting')
plt.legend()
plt.grid(True)

# 保存图像
plt.savefig('power_function_fit.png')
plt.close()