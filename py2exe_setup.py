# mysetup.py
# from distutils.core import setup
# import py2exe

# setup(console=["helloworld.py"])

# -*- encoding:utf-8 -*-

from distutils.core import setup
import py2exe

INCLUDES = []

options = {
    "py2exe":
        {
            "compressed": 1,  # 压缩
            "optimize": 2,
            "bundle_files": 1,  # 所有文件打包成一个 exe 文件
            "includes": INCLUDES,
            "dll_excludes": ["MSVCR100.dll"]
        }
}

setup(
    options=options,
    description="this is a py2exe test",
    zipfile=None,
    data_files=[
        ('', [r'C:\Users\54067\AppData\Roaming\Python\Python310\site-packages\pyecharts\datasets\map_filename.json', ]),
        ("", ["MSVCR100.dll"]),
    ],
    py_modules=[r'E:\lib4python\yangke\game'],
    console=[{"script": r'E:\lib4python\yangke\game\gf_ui.py'}],
)
