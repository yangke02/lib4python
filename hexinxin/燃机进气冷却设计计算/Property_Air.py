import numpy as np


# =========================函数
# =========================已知空气压力、干球温度、相对湿度，求其他参数
def d_PTRH(press, temper, rh):
    pg = Cal_Pg(temper)
    pq = rh * 0.01 * pg
    D = Cal_D(pq, press)
    return D


def h_PTRH(press, temper, rh):
    dd = d_PTRH(press, temper, rh)
    hh = Cal_H(temper, dd)
    return hh


def td_PTRH(press, temper, rh):
    # dd = d_PTRH(press, temper, rh)
    # pg = dd * press * 1000 / (dd + 621.945)
    # temper = Cal_Td(pg)
    temper = 237.7 * ((17.27 * temper) / (237.7 + temper) + np.log(rh / 100)) / (
                17.27 - ((17.27 * temper) / (237.7 + temper) + np.log(rh / 100)))
    return temper


# =========================已知空气压力、干球温度、含湿量，求其他参数
def h_PTD(press, temper, d0):
    hh = Cal_H(temper, d0)
    return hh


def rh_PTD(press, temper, d0):
    pg = Cal_Pg(temper)
    pq = d0 * press * 1000 / (d0 + 621.945)
    rh = pq / pg * 100
    return rh


def td_PTD(press, temper, d0):
    pq = d0 * press * 1000 / (d0 + 621.945)
    td = Cal_Td(pq)
    return td


# =========================已知空气压力、干球温度、露点温度，求其他参数
def h_PTTd(press, temper, td0):
    pq = Cal_pq3(td0)
    dd = Cal_D(pq, press)
    hh = Cal_H(temper, dd)
    return hh


def d_PTTd(press, temper, td0):
    pq = Cal_pq3(td0)
    dd = Cal_D(pq, press)
    return dd


def rh_PTTd(press, temper, td0):
    pq = Cal_pq3(td0)
    pg = Cal_Pg(temper)
    rh = pq / pg * 100
    return rh


# =========================基础函数
# 已知温度求水蒸气饱和压力Pg
def Cal_Pg(temper):
    if temper < 0:
        Pg = np.exp((-5674.5359 / (273.15 + temper)) + 6.3925247 + ((-0.9677843 * (0.01)) * (273.15 + temper)) + (
                (0.62215701 * (0.000001)) * ((273.15 + temper) ** 2))
                    + ((0.20747825 * (0.00000001)) * ((273.15 + temper) ** 3)) + (
                            (-0.9484024 * (0.000000000001)) * ((273.15 + temper) ^ 4)) + 4.1635019 * np.log(
            273.15 + temper))
    else:
        Pg = np.exp((-5800.2206 / (273.15 + temper)) + 1.3914993 + (-0.04860239 * (273.15 + temper)) + (
                (0.41764768 * (0.0001)) * ((273.15 + temper) ** 2))
                    + ((-0.14452093 * (0.0000001)) * ((273.15 + temper) ** 3)) + 6.5459673 * np.log(273.15 + temper))
    return Pg


# 已知水蒸气分压力Pg和大气压力press求含湿量D
def Cal_D(press, press0):
    D = 621.945 * press / (1000 * press0 - press)
    return D


# 已知干球温度temper和含湿量D求焓值H
def Cal_H(temper, den):
    H = 1.006 * temper + 0.001 * den * (2501 + 1.86 * temper)
    return H


# 已知水蒸气分压力Pq求露点温度td
def Cal_Td(pre):
    B = -60.45 + 7.0322 * (np.log(pre)) + 0.37 * ((np.log(pre)) ** 2)
    BB = -35.957 - 1.8726 * (np.log(pre)) + 1.1689 * ((np.log(pre)) ** 2)
    if B < 0:
        return B
    else:
        return BB


# 已知空气压力、干球温度、水蒸气分压力，求密度rho
def Cal_rho(press, temper, pq):
    rho = (0.00348 * press * 1000 / (temper + 273.15)) - (0.00132 * pq / (temper + 273.15))
    return rho


# 求Pq
def Cal_pq1(press, temper, ts):
    pg = Cal_Pg(temper)
    pq = pg - 0.00001 * (65 + 6.75 / 5) * (temper - ts) * press * 1000
    return pq


def Cal_pq2(ts):
    if ts < 0:
        pq = np.exp((1.8726 + (1.8726 * 1.8726 + 4 * 1.1689 * (35.957 + ts)) ** 0.5) / (2 * 1.1689))
    else:
        pq = np.exp((1.8726 + (1.8726 * 1.8726 + 4 * 1.1689 * (35.957 + ts)) ** 0.5) / (2 * 1.1689))
    return pq


def Cal_pq3(td):
    if td < 0:
        pq = np.exp((-7.0322 + ((7.0322 * 7.0322) + 4 * 0.37 * (60.45 + td)) ** 0.5) / (2 * 0.37))
    else:
        pq = np.exp((1.8726 + (1.8726 * 1.8726 + 4 * 1.1689 * (35.957 + td)) ** 0.5) / (2 * 1.1689))
    return pq
