import sys
import matplotlib
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QVBoxLayout, QLabel, QHBoxLayout, QPushButton, QDialog, QLineEdit, \
    QGridLayout, QMessageBox, QComboBox, QTableWidget,QApplication

matplotlib.use("Qt5Agg")
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
import numpy as np
from Cal_TICA import cal_TICA_from_t1, cal_TICA_from_delta_power_net, curve_wetair, curve_TICA_process


def get_data_wetair(p0=101.0):
    data_wetair = curve_wetair(p0)  # 湿空气焓湿图t-d数据组
    return data_wetair


def set_plot(title, xlabel, ylabel, size0, size1, size2):
    plt.title(title, size=size0)
    plt.xticks(fontsize=size1)
    plt.yticks(fontsize=size1)
    plt.xlabel(xlabel, size=size2)
    plt.ylabel(ylabel, size=size2)
    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.rcParams['axes.unicode_minus'] = False


def plot_origin(x, y, xmin, xmax, ymin, ymax, label):
    plt.plot(x, y, label=label)
    plt.xlim(xmin, xmax)
    plt.ylim(ymin, ymax)
    plt.grid(visible=True, which='major', axis='both')
    plt.draw()


class PlotWavelet(FigureCanvas):
    def __init__(self):
        self.fig = plt.figure()  # linewidth=1
        FigureCanvas.__init__(self, self.fig)

    def reset(self):
        self.fig.clear()
        # self.axes = self.fig.add_subplot(111)


class WaveletDialog(QDialog):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        font1 = QFont('Arial', weight=80)  # pointSize=10
        # ==================================================
        # parameters
        layoutPara = QGridLayout()  # QHBoxLayout
        i = 2

        # 输入参数
        self.input = QLabel('输入参数')
        self.input.setFont(font1)
        layoutPara.addWidget(self.input, 1, 0)

        # 输出参数
        self.input = QLabel('输出参数')
        self.input.setFont(font1)
        layoutPara.addWidget(self.input, i + 8, 0)

        # 添加表格
        name_t0 = ['大气温度', '℃']
        name_p0 = ['大气压力', 'kPa']
        name_rh0 = ['大气湿度', '%']
        name = [name_t0, name_p0, name_rh0]
        name = np.array(name)

        self.myTable = QTableWidget()
        layoutPara.addWidget(self.myTable, i + 9, 0, 4, 2)
        self.myTable.setColumnCount(3)
        self.myTable.setRowCount(3)
        self.myTable.setHorizontalHeaderLabels(['参数', '单位', '数值'])
        self.myTable.setVerticalHeaderLabels(name[:, 0])
        # self.myTable.setItem(3, 3, name)


        # 添加选择框
        self.box = QComboBox()
        self.box.addItems(['计算模式', '输入冷却后温度', '输入功率增量'])
        layoutPara.addWidget(self.box, 0, 0, 1, 2)

        # 添加参数名称和输入框
        self.labelP0 = QLabel('大气压力(kPa)')
        self.lineeditP0 = QLineEdit('101')
        layoutPara.addWidget(self.labelP0, i, 0)
        layoutPara.addWidget(self.lineeditP0, i, 1)

        self.labelT0 = QLabel('大气温度(℃)')
        self.lineeditT0 = QLineEdit('35')
        layoutPara.addWidget(self.labelT0, i + 1, 0)
        layoutPara.addWidget(self.lineeditT0, i + 1, 1)

        self.labelRH0 = QLabel('大气湿度(%)')
        self.lineeditRH0 = QLineEdit('70')
        layoutPara.addWidget(self.labelRH0, i + 2, 0)
        layoutPara.addWidget(self.lineeditRH0, i + 2, 1)

        self.labeW0 = QLabel('机组功率(MW)')
        self.lineeditW0 = QLineEdit('61.467')
        layoutPara.addWidget(self.labeW0, i + 3, 0)
        layoutPara.addWidget(self.lineeditW0, i + 3, 1)

        self.labeF0 = QLabel('空气流量(t/h)')
        self.lineeditF0 = QLineEdit('400')
        layoutPara.addWidget(self.labeF0, i + 4, 0)
        layoutPara.addWidget(self.lineeditF0, i + 4, 1)

        self.labeCOP = QLabel('能效系数')
        self.lineeditCOP = QLineEdit('4.5')
        layoutPara.addWidget(self.labeCOP, i + 5, 0)
        layoutPara.addWidget(self.lineeditCOP, i + 5, 1)

        self.labeT1 = QLabel('目标温度（℃）')
        self.lineeditT1 = QLineEdit('25')
        layoutPara.addWidget(self.labeT1, i + 6, 0)
        layoutPara.addWidget(self.lineeditT1, i + 6, 1)

        self.labeW1 = QLabel('功率增量（MW）')
        self.lineeditW1 = QLineEdit('3')
        layoutPara.addWidget(self.labeW1, i + 7, 0)
        layoutPara.addWidget(self.lineeditW1, i + 7, 1)

        self.condition0()
        self.box.currentIndexChanged.connect(self.set_condition)

        # ==================================================
        # figure-1: wave
        self.plotWave = PlotWavelet()

        # ==================================================
        # put figures into layout
        layoutPaintCol = QVBoxLayout()

        # layoutPaintCol.addWidget(self.plotWave,0,4,4,4)
        layoutPara.addWidget(self.plotWave, 0, 2, 18, 18)

        # ==================================================
        # ok and cancel
        buttonOK = QPushButton('计算')
        buttonOK.clicked.connect(self.plot1)
        buttonCancel = QPushButton('取消')
        buttonCancel.clicked.connect(self.close)
        layoutOkCancel = QHBoxLayout()
        layoutOkCancel.addStretch(1)
        layoutOkCancel.addWidget(buttonOK)
        layoutOkCancel.addWidget(buttonCancel)

        # ==================================================
        # top layout
        layoutTop = QVBoxLayout(self)
        layoutTop.addLayout(layoutPara)
        layoutTop.addLayout(layoutPaintCol)
        layoutTop.addLayout(layoutOkCancel)

        self.plot0()
        self.resize(1000, 600)
        self.setWindowTitle('燃气轮机进气冷却系统设计软件')
        self.show()

    def set_condition(self):
        index = self.box.currentIndex()
        if index == 0:
            self.condition0()
        if index == 1:
            self.condition1()
        if index == 2:
            self.condition2()

    def condition0(self):
        self.lineeditT1.setReadOnly(True)
        self.lineeditT1.setStyleSheet("background-color:#F0F0F0")
        self.lineeditW1.setReadOnly(True)
        self.lineeditW1.setStyleSheet("background-color:#F0F0F0")

    def condition1(self):
        self.lineeditT1.setReadOnly(False)
        self.lineeditT1.setStyleSheet("background-color:#FFFFFF")
        self.lineeditW1.setReadOnly(True)
        self.lineeditW1.setStyleSheet("background-color:#F0F0F0")

    def condition2(self):
        self.lineeditT1.setReadOnly(True)
        self.lineeditT1.setStyleSheet("background-color:#F0F0F0")
        self.lineeditW1.setReadOnly(False)
        self.lineeditW1.setStyleSheet("background-color:#FFFFFF")

    def plot0(self):
        # 画空气焓湿线
        label = ["100%RH", "90%RH", "80%RH", "70%RH", "60%RH", "50%RH", "40%RH", "30%RH", "20%RH", "10%RH"]  # 定义标签
        x = np.linspace(0, 50, 26)
        data_wetair = get_data_wetair(101)

        set_plot("空气焓湿图", "空气温度（℃）", "空气含湿量（kg/kg）", 18, 13, 13)  # 设置坐标轴参数
        for i in range(10):
            plot_origin(x, data_wetair[:, i], 10, 50, 0, 0.06, label[i])
        plt.plot([0, 0], [-1, -1], color="black", linewidth=2, label="冷却过程线")  # 冷却过程线
        plt.scatter(0, 0.1, 50, color="red", label="冷却起点")  # 冷却起点
        plt.scatter(0, 0.1, 50, color="green", label="冷却终点")  # 画冷却终点
        plt.legend(frameon=True, loc='upper left', fontsize='small')

    def plot1(self):
        p0 = float(self.lineeditP0.text())
        t0 = float(self.lineeditT0.text())
        rh0 = float(self.lineeditRH0.text())
        f_air0 = float(self.lineeditF0.text())
        w0 = float(self.lineeditW0.text())
        cop = float(self.lineeditCOP.text())
        t1 = float(self.lineeditT1.text())
        delta_power_net = float(self.lineeditW1.text())
        f_gas0 = 14500
        w_st0 = 19.116
        self.plotWave.reset()

        if self.box.currentIndex() == 0:
            QMessageBox.warning(self, '警告', '请选择计算模式！', QMessageBox.Ok)
        else:
            if self.box.currentIndex() == 1:
                if t1 > t0 or t1 > 50 or t1 < 10 or p0 > 115 or p0 < 85:
                    QMessageBox.warning(self, '警告', '计算范围不常用，计算结果仅供参考，建议重新输入！', QMessageBox.Ok)

                res_from_t1 = cal_TICA_from_t1(p0, t0, rh0, f_air0, f_gas0, w0, w_st0, cop,
                                               t1)  # 输入目标温度，求解p0,t0,d0,rh0,t1,d1,rh1,td,delta_power_net,q_cop,delta_temper,f_water,condition
                data_TICA = curve_TICA_process(res_from_t1[0], res_from_t1[1], res_from_t1[2], res_from_t1[4],
                                               res_from_t1[5],
                                               res_from_t1[7])  # 求解冷却过程线t-d数据组
            else:
                res_from_w1 = cal_TICA_from_delta_power_net(p0, t0, rh0, f_air0, f_gas0, w0, w_st0, cop,
                                                            delta_power_net)  # 输入目标净功率，求解p0,t0,d0,rh0,t1,d1,rh1,td,delta_power_net,q_cop,delta_temper,f_water,condition
                data_TICA = curve_TICA_process(res_from_w1[0], res_from_w1[1], res_from_w1[2],
                                               res_from_w1[4], res_from_w1[5],
                                               res_from_w1[7])  # 求解冷却过程线t-d数据组

            # 画空气焓湿线
            label = ["100%RH", "90%RH", "80%RH", "70%RH", "60%RH", "50%RH", "40%RH", "30%RH", "20%RH", "10%RH"]  # 定义标签
            x = np.linspace(0, 50, 26)
            data_wetair = get_data_wetair(p0)

            set_plot("空气焓湿图", "空气温度（℃）", "空气含湿量（kg/kg）", 18, 13, 13)  # 设置坐标轴参数
            for i in range(10):
                plot_origin(x, data_wetair[:, i], 10, 50, 0, 0.06, label[i])  # 等RH曲线
            plt.plot(data_TICA[:, 0], data_TICA[:, 1], color="black", linewidth=2, label="冷却过程线")  # 冷却过程线
            plt.scatter(data_TICA[0, 0], data_TICA[0, 1], 50, color="red", label="冷却起点")  # 冷却起点
            plt.scatter(data_TICA[11, 0], data_TICA[11, 1], 50, color="green", label="冷却终点")  # 冷却终点
            plt.legend(frameon=True, loc='upper left', fontsize='small')  # 分别为图例无边框、图例放在左上角、图例大小

    def table(self):
        print('h')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    test = WaveletDialog()
    sys.exit(app.exec_())

