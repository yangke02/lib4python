# ==========主程序
data_wetair = curve_wetair()  # 湿空气焓湿图t-d数据组

res_from_t1 = cal_TICA_from_t1(p0=101, t0=35, rh0=70, f_air0=400, f_gas0=14500, w0=61.647, w_st0=19.116, cop=4,
                               t1=25)  # 输入目标温度，求解p0,t0,d0,rh0,t1,d1,rh1,td,delta_power_net,q_cop,delta_temper,f_water,condition

res_from_net_power = cal_TICA_from_delta_power_net(p0=101, t0=35, rh0=70, f_air0=400, f_gas0=14500, w0=61.647,
                                                   w_st0=19.116, cop=4,
                                                   delta_power_net=5)  # 输入目标净功率，求解p0,t0,d0,rh0,t1,d1,rh1,td,delta_power_net,q_cop,delta_temper,f_water,condition

# data_TICA = curve_TICA_process(res_from_t1[0], res_from_t1[1], res_from_t1[2], res_from_t1[4], res_from_t1[5], res_from_t1[7])
data_TICA = curve_TICA_process(res_from_net_power[0], res_from_net_power[1], res_from_net_power[2],
                               res_from_net_power[4], res_from_net_power[5],
                               res_from_net_power[7])  # 输入p0, t0, t1, d0, d1, td，求解冷却过程线t-d数据组

# ==========空气焓湿图设置
plt.rcParams['font.sans-serif'] = ['SimHei']  # 正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 正常显示负号

plt.figure(figsize=(16, 12), dpi=100)  # 设置图表的宽高比为 10:6，设置 dpi 为 80
plt.title('燃气轮机进气冷却系统设计-空气焓湿图', size=35)  # 设置图片标题

plt.xlim(10, 50)  # 设置x坐标轴范围
plt.ylim(0, 0.035)  # 设置y坐标轴范围
plt.xlabel("空气温度（℃）", size=20)  # 设置x坐标轴名称、字体大小
plt.ylabel("空气含湿量（kg/kg）", size=20)  # 设置y坐标轴名称、字体大小
plt.xticks(fontsize=20)  # 设置x轴刻度字体大小
plt.yticks(fontsize=20)  # 设置y轴刻度字体大小

# ==========画空气焓湿图
label = ["100%RH", "90%RH", "80%RH", "70%RH", "60%RH", "50%RH", "40%RH", "30%RH", "20%RH", "10%RH"]  # 定义标签
x = np.linspace(0, 50, 26)
for i in range(10):
    plt.plot(x, data_wetair[:, i], linewidth=2.0, label=label[i])  # 画空气焓湿线

# ==========画冷却曲线
plt.plot(data_TICA[:, 0], data_TICA[:, 1], color="black", linewidth=3, label="冷却过程线")  # 画冷却过程线
plt.scatter(data_TICA[0, 0], data_TICA[0, 1], 150, color="red", label="冷却起点")  # 画冷却起点
plt.scatter(data_TICA[11, 0], data_TICA[11, 1], 150, color="green", label="冷却终点")  # 画冷却终点

# ==========画图例
plt.legend(frameon=True, loc='upper left', fontsize='x-large')  # 分别为图例无边框、图例放在左上角、图例大小

plt.show()  # 显示图片
