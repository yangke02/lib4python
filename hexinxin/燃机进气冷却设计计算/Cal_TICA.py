import numpy as np

from Property_Air import d_PTRH, h_PTD, h_PTRH, rh_PTD, td_PTRH


def curve_wetair(p0=101):  # 湿空气焓湿图数据
    data = [[0.0] * 10] * 26  # 列表的形式定义一个包含 26行 10列的二维数组
    data = np.array(data)  # 使用numpy中的array，将列表转化为标准的数组：
    i = 0

    for temper in range(0, 51, 2):
        i = i + 1
        j = 0
        for rh in range(100, 0, -10):
            data[i - 1, j] = d_PTRH(p0, temper, rh) / 1000
            j = j + 1
    return data


def curve_TICA_process(p0, t0, d0, t1, d1, td):
    data_TICA = [[0.0] * 2] * 12  # 列表的形式定义一个包含 11行 2列的二维数组
    data_TICA = np.array(data_TICA)  # 使用numpy中的array，将列表转化为标准的数组：
    data_TICA[0] = [t0, d0]
    if t1 < td:
        data_TICA[1] = [td, d0]
        tt1 = td
        data_TICA[11] = [t1, d1]
    else:
        data_TICA[1] = [t0, d0]
        tt1 = t0
        data_TICA[11] = [t1, d0]

    for i in range(9):
        if t1 < td:
            data_TICA[i + 2] = [tt1 - (tt1 - t1) / 9 * (i + 1), d_PTRH(p0, tt1 - (tt1 - t1) / 9 * (i + 1), 100) / 1000]
        else:
            data_TICA[i + 2] = [t0, d0]

    return data_TICA


def cal_TICA_from_delta_power_net(p0=101, t0=35, rh0=70, f_air0=400, f_gas0=14500, w0=61.647, w_st0=19.116, cop=4,
                                  delta_power_net=3):
    coefficient_auxi = [-0.02164400, 0.16521000]
    coefficient_power = [-0.00009282, -0.00056910, 1.04520000]
    temper = t0
    power = delta_power_net

    while (power - delta_power_net) < 0.001:
        _ = cal_TICA_from_t1(p0, t0, rh0, f_air0, f_gas0, w0, w_st0, cop, temper)
        power = _[8]
        temper = temper - 0.005
        if temper < 5:
            break
    return _


def cal_TICA_from_t1(p0=101, t0=35, rh0=70, f_air0=400, f_gas0=14500, w0=61.647, w_st0=19.116, cop=4, t1=25):
    coefficient_auxi = [-0.02164400, 0.16521000]
    coefficient_power = [-0.00009282, -0.00056910, 1.04520000]

    td = td_PTRH(p0, t0, rh0)  # 进口空气温度对应露点温度
    d0 = d_PTRH(p0, t0, rh0)  # 进口空气含湿量
    if t1 > td:  # 如果出口空气温度>露点温度
        rh1 = rh_PTD(p0, t0, d0)
        condition = "等湿降温"
    else:
        rh1 = 100
        condition = "除湿降温"
    d1 = d_PTRH(p0, t1, rh1)
    h0 = h_PTRH(p0, t0, rh0)
    h1 = h_PTD(p0, t1, d1)
    q_cop = f_air0 * (h0 - h1) / 3600
    correct_power0 = coefficient_power[0] * t0 * t0 + coefficient_power[1] * t0 + coefficient_power[2]
    correct_power1 = coefficient_power[0] * t1 * t1 + coefficient_power[1] * t1 + coefficient_power[2]
    delta_power_cop = q_cop / cop
    delta_power_auxi = coefficient_auxi[0] * q_cop * q_cop + coefficient_auxi[1] * q_cop
    delta_power_unit = w0 / correct_power0 - w0 / correct_power1
    delta_power_net = delta_power_unit - delta_power_auxi - delta_power_cop
    delta_temper = t0 - t1
    f_water = f_air0 * (d0 - d1) / 1000
    _ = p0, t0, d0 / 1000, rh0, t1, d1 / 1000, rh1, td, delta_power_net, q_cop, delta_temper, f_water, condition
    return _
