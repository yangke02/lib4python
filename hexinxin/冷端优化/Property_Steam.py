from iapws import iapws97  # IAPWS-IF97——水蒸汽,IAPWS-95——水蒸汽,IAPWS-06——冰,IAPWS-08——海水,IAPWS-17—— 重水


# =========================已知T
def p_T(temper):
    temper = temper + 273.15
    cal = iapws97.IAPWS97_Tx(temper, 0)
    p = cal.P
    return p


# =========================已知P
def t_P(press):
    cal = iapws97.IAPWS97_Px(press, 1)  # x=1为蒸汽
    t = cal.T - 273.15
    return t


def h_P(press):
    cal = iapws97.IAPWS97_Px(press, 1)
    h = cal.h
    return h


def s_P(press):
    cal = iapws97.IAPWS97_Px(press, 1)
    s = cal.s
    return s


def v_P(press):
    cal = iapws97.IAPWS97_Px(press, 1)
    v = cal.v
    return v


def tw_P(press):
    cal = iapws97.IAPWS97_Px(press, 0)  # x=0为水
    T = cal.T - 273.15
    return T


def hw_P(press):
    cal = iapws97.IAPWS97_Px(press, 0)
    h = cal.h
    return h


def sw_P(press):
    cal = iapws97.IAPWS97_Px(press, 0)
    s = cal.s
    return s


# =========================已知PT
def h_PT(press, temper):
    cal = iapws97.IAPWS97_PT(press, temper + 273.15)
    h = cal.h
    return h


def s_PT(press, temper):
    cal = iapws97.IAPWS97_PT(press, temper + 273.15)
    s = cal.s
    return s


def v_PT(press, temper):
    cal = iapws97.IAPWS97_PT(press, temper + 273.15)
    v = cal.v
    return v


def cp_PT(press, temper):
    cal = iapws97.IAPWS97_PT(press, temper + 273.15)
    cp = cal.v
    return cp


def rho_PT(press, temper):
    cal = iapws97.IAPWS97_PT(press, temper + 273.15)
    rho = 1 / cal.v
    return rho


# =========================已知PS
def h_PS(press, entropy):
    cal = iapws97.IAPWS97_Ps(press, entropy)
    h = cal.h
    return h


def v_PS(press, entropy):
    cal = iapws97.IAPWS97_Ps(press, entropy)
    v = cal.v
    return v


def s_PS(press, entropy):
    cal = iapws97.IAPWS97_Ps(press, entropy)
    h = cal.h
    return h


# =========================已知PH
def x_PH(press, enthalpy):
    cal = iapws97.IAPWS97_Ph(press, enthalpy)
    x = cal.x
    return x


def v_PH(press, enthalpy):
    cal = iapws97.IAPWS97_Ph(press, enthalpy)
    v = cal.v
    return v


if __name__ == '__main__':
    # xx = x_PH(0.009, 2400)
    xx = p_T(49)
    # print(xx)
