"""
程序功能：进行校验目前的凝汽器压力在线计算（认为清洁系数不变）+冷却塔拟合计算程序，其中冷却塔拟合计算结果采用绝对值
程序作者：何欣欣
程序生成日期：2023年8月2日
最近修改日期：
最近修改内容：
"""
import sys

sys.path.append("..")  # 跳到上级目录下面
from Cal_03_condenser.cal_condenser import cal_factor_cleanliness
from Flow_from_num_unit_pump_tower import flow_cw_from_pump_and_tower

# =================定义通用参数
cp_water = 4.186  # 水比热
pi = 3.14159  # 圆周率

t_atm = 11.619
rh_atm = 58.1
p_atm = 98.29
t_in = 25.05
t_out = 37.05
p_cond = 6.4
num_unit = 1
num_pump = 2
num_tower = 4


# ================计算
def cal_off(press_atm, temper_atm, humid_atm, p_condensr, t_in_condensr, t_out_condensr, number_unit, number_pump,
            number_tower):
    f_cw = flow_cw_from_pump_and_tower(num_unit, num_pump)  # 循环水流量
    f_tower = f_cw / num_tower  # 机力塔流量
    q_tower = cp_water * (t_out_condensr - t_in_condensr) * f_tower / 3600  # 机力热负荷

    t_out_tower = -0.0214394 * press_atm * 10 + 0.06162906 * humid_atm + 0.45512196 * temper_atm + 0.00117998\
                  * f_tower + 0.06597517 * q_tower + 27.3418719279675  # 多元拟合公式求冷却塔出水温度

    factor_cond_0, q_cond_0, p0 = cal_factor_cleanliness(p_condensr, t_in_condensr, t_out_condensr,
                                                         f_cw)  # 计算凝汽器清洁系数和热负荷
    factor_cond_1, q_cond_1, p1 = cal_factor_cleanliness(p_condensr, t_out_tower, t_out_condensr, f_cw)  # 计算凝汽器清洁系数和热负荷

    print(factor_cond_1, q_cond_1, p0)


if __name__ == '__main__':
    cal_off(p_atm, t_atm, rh_atm, p_cond, t_in, t_out, num_unit, num_pump, num_tower)
