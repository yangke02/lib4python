"""
程序功能：机力通风冷却塔变工况热力计算
程序作者：何欣欣
程序生成日期：2023年8月2日
最近修改日期：
最近修改内容：
"""
# =================定义通用参数
cp_water = 4.186  # 水比热
pi = 3.14159  # 圆周率

area_materal = 18 * 19  # 填料面积（m2）
factor_ATO = 1.704283  # 热力特性（S波填料）-根据运行数据拟合得到结果
factor_MTO = 0.4076
# CT_R_FAN_SP = [72.5378 114.487 145.076 171.294 200.571 222.42 237.714 247.765 254.756 257.815]
# CT_R_FAN_F = [833.229 761.622 695.632 635.257 555.226 479.407 416.225 360.062 294.072 251.95]


# ================计算
def cal_(p_atm, rh_atm, t_atm, t_in, t_out):
    p0 = p_atm * 100
    rh0 = rh_atm / 100
    flow_cw = flow_cw_from_pump(index=1)
    t_rise = t_out - t_in
    q_cond = t_rise * cp_water * flow_cw / 3600

    dp_tower = 140  # 整塔的阻力系数（暂取固定值）
    return  # 阻力特性系数，℃,塔内风速  ,冷却塔水流量，t/h,冷却塔热负荷


# =================根据泵运行方式选择循环水流量(自定义)
def flow_cw_from_pump(index=1):
    if index == 1:
        ff = 22664  # 一机三泵
    elif index == 2:
        ff = 17949  # 一机两泵
    else:
        ff = 8780  # 一机一泵
    return ff


# ================计算
def cal_flow(num_pump, num_tower):
    return  # 阻力特性系数，℃,塔内风速  ,冷却塔水流量，t/h,冷却塔热负荷
