# =================根据泵运行方式选择循环水流量(自定义)
def flow_cw_from_pump(num_pump=1):
    if num_pump == 3:
        ff = 22664  # 一机三泵
    elif num_pump == 2:
        ff = 17949  # 一机两泵
    else:
        ff = 8780  # 一机一泵
    return ff


def flow_cw_from_pump_and_tower(num_unit=2, num_pump=1):
    if num_unit == 2:  # 两机
        if num_pump == 6:
            ff = 44828  # 两机六泵
        elif num_pump == 5:
            ff = 39724  # 两机五泵
        elif num_pump == 4:
            ff = 39724  # 两机四泵
        elif num_pump == 3:
            ff = 22664  # 两机三泵
        else:
            ff = 17949  # 两机两泵
        return ff
    else:
        if num_pump == 3:
            ff = 22664  # 一机三泵
        elif num_pump == 2:
            ff = 17949  # 一机两泵
        else:
            ff = 8780  # 一机一泵
        return ff
