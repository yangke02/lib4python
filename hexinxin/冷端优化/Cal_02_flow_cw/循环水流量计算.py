from Property_Steam import h_PT, hw_P, s_PT, h_PS, v_PH, x_PH

"""
程序功能：热平衡法计算循环水流量，机组形式包括：
1、单缸双压汽轮机（多轴）
2、三缸三压汽轮机（分缸、多轴）
3、三缸三压汽轮机（合缸、多轴）
4、三缸三压汽轮机（合缸、单轴）
程序作者：何欣欣
程序生成日期：2023年8月2日
最近修改日期：
最近修改内容：
"""

# =================定义通用参数
cp_water = 4.186  # 水比热
eta_m = 0.998  # 机械效率
eta_e = 0.99  # 电气效率


# =================单缸双压汽轮机（多轴）
def cal_flow_cw_singlecylinder_doublepressure_multishaft(p_ms, t_ms, f_ms, p_ls, t_ls, f_ls, power_st, t_in_cw,
                                                         t_out_cw):
    f_valve = 1  # 门杆漏汽量
    f_seal_front = 1  # 高压前轴封漏汽量
    h_ms = h_PT(p_ms, t_ms)
    h_ls = h_PT(p_ls, t_ls)
    # cp_water = cp_PT(1, (t_in_cw + t_out_cw) / 2)
    q_in = h_ms * (f_ms - f_valve - f_seal_front) + h_ls * f_ls  # 进入汽轮机能量,kJ/h
    q_out = power_st * 3600 / eta_e / eta_m  # 汽轮机输出轴功率,kJ/h
    flow_cw = (q_in - q_out) / (t_out_cw - t_in_cw) / cp_water  # 计算循环水流量
    return flow_cw


# =================三缸三压汽轮机（分缸、多轴）
def cal_flow_cw_threecylinder_threepressure_split_multishaft(p_ms, t_ms, f_ms, p_ex_HP, t_ex_HP, p_is, t_is, f_is, p_ls,
                                                             t_ls,
                                                             f_ls, power_st, t_in_cw, t_out_cw):
    f_valve = 0  # 门杆漏汽量
    f_seal_front = 6  # 高压前轴封漏汽量
    f_seal_rear = 2  # 高压后轴封漏汽量
    h_ms = h_PT(p_ms, t_ms)
    h_ex_HP = h_PT(p_ex_HP, t_ex_HP)
    h_is = h_PT(p_is, t_is)
    h_ls = h_PT(p_ls, t_ls)
    f_ex_HP = f_ms - f_valve - f_seal_front - f_seal_rear
    q_in = h_ms * (f_ms - f_valve - f_seal_front) - h_ex_HP * f_ex_HP + h_is * f_is + h_ls * f_ls  # 进入汽轮机能量,kJ/h
    q_out = power_st * 3600 / eta_e / eta_m  # 汽轮机输出轴功率,kJ/h
    flow_cw = (q_in - q_out) / (t_out_cw - t_in_cw) / cp_water  # 计算循环水流量
    return flow_cw


# =================三缸三压汽轮机（合缸、多轴）
def cal_flow_cw_threecylinder_threepressure_HPIPcombine_multishaft(p_ms, t_ms, f_ms, p_ex_HP, t_ex_HP, p_is, t_is, f_is,
                                                                   p_ls, t_ls, f_ls, power_st, t_in_cw, t_out_cw):
    f_valve = 1  # 门杆漏汽量
    f_seal_front = 6  # 高压前轴封漏汽量
    f_seal_rear = 2  # 高压后轴封漏汽量
    f_seal_HPIP_percentage = 1.2  # 合缸漏汽百分比
    h_ms = h_PT(p_ms, t_ms)
    h_ex_HP = h_PT(p_ex_HP, t_ex_HP)
    h_is = h_PT(p_is, t_is)
    h_ls = h_PT(p_ls, t_ls)
    f_ex_HP = (f_ms - f_valve - f_seal_front - f_seal_rear) * (1 - f_seal_HPIP_percentage / 100)

    q_in = h_ms * (f_ms - f_seal_front) - h_ex_HP * f_ex_HP + h_is * f_is + h_ls * f_ls  # 进入汽轮机能量,kJ/h
    q_out = power_st * 3600 / eta_e / eta_m  # 汽轮机输出轴功率,kJ/h
    flow_cw = (q_in - q_out) / (t_out_cw - t_in_cw) / cp_water  # 计算循环水流量
    return flow_cw


# =================三缸三压汽轮机（合缸、单轴）
def cal_flow_cw_threecylinder_threepressure_HPIPcombine_singleshaft(p_in_LP, t_in_LP, f_ms, f_is, f_ls, t_in_cw,
                                                                    t_out_cw,
                                                                    eff_LP_design, p_ex_LP):
    # f_valve = 0  # 门杆漏汽量
    # f_seal_front = 0  # 高压前轴封漏汽量
    # f_seal_rear = 0  # 高压后轴封漏汽量
    # f_seal_HPIP_percentage = 1.2  # 合缸漏汽百分比
    # # f_ex_HP = (f_ms - f_valve - f_seal_front - f_seal_rear) * (1 - f_seal_HPIP_percentage / 100)  # 高压后排汽汽量

    f_in_LP = f_ms + f_ls + f_is  # 低压缸进汽流量
    h_in_LP = h_PT(p_in_LP, t_in_LP)  # 低压缸进汽焓
    # v_in_LP = v_PT(p_in_LP, t_in_LP)  # 低压缸进汽比容
    s_in_LP = s_PT(p_in_LP, t_in_LP)  # 低压缸进汽熵
    hds_in_LP = h_PS(p_ex_LP / 1000, s_in_LP)  # 低压缸等熵焓
    h_ex_LP_EEEP = h_in_LP - eff_LP_design * (h_in_LP - hds_in_LP) / 100  # 膨胀线终点焓ELEP
    v_ex_LP_ELEP = v_PH(p_ex_LP / 1000, h_ex_LP_EEEP)  # 膨胀线终点蒸汽比容
    x_ex_LP_ELEP = x_PH(p_ex_LP / 1000, h_ex_LP_EEEP)  # 膨胀线终点蒸汽干度
    y_ex_LP_ELEP = 1 - x_ex_LP_ELEP  # 膨胀线终点蒸汽湿度
    vdry_ex_LP_ELEP = v_ex_LP_ELEP / (1 - y_ex_LP_ELEP)  # 膨胀线终点干蒸汽的比体积
    fmass_ex_LP = f_in_LP * (1 - y_ex_LP_ELEP)  # 膨胀线终点干蒸汽的质量流量
    fvolumn_ex_LP = vdry_ex_LP_ELEP * fmass_ex_LP  # 膨胀线终点干蒸汽的体积流量m3/s
    qloss_dry = 0.00001497 * fvolumn_ex_LP ** 2 - 0.049831304 * fvolumn_ex_LP + 55.4022778329  # 根据曲线查排汽损失kJ/kg
    # qloss = qloss_dry * 0.87 * (1 - y_ex_LP_ELEP) * (1 - 0.65 * y_ex_LP_ELEP)  # 排汽损失（含湿度修正）kJ/kg
    h_ex_LP_UEEP = h_ex_LP_EEEP + 0.087 * (1 - y_ex_LP_ELEP) * qloss_dry  # 有用能终点焓UEEP
    h_hw = hw_P(p_ex_LP / 1000)  # 热井出水焓
    q_ex_LP = f_in_LP * (h_ex_LP_UEEP - h_hw)  # 凝汽器换热量
    flow_cw = q_ex_LP / (t_out_cw - t_in_cw) / cp_water  # 计算循环水流量
    return flow_cw


if __name__ == '__main__':
    f1 = cal_flow_cw_singlecylinder_doublepressure_multishaft(5.71, 535, 67.09, 0.559, 249.9, 8.32, 21.339, 20, 30)
    f2 = cal_flow_cw_threecylinder_threepressure_split_multishaft(15.2, 565, 312.7, 4.01, 359.4, 3.70, 565, 355.7, 0.42,
                                                                  244.6, 39.52, 159.13, 30, 36.6)
    f3 = cal_flow_cw_threecylinder_threepressure_HPIPcombine_multishaft(15.2, 565, 312.7, 4.01, 359.4, 3.70, 565, 355.7,
                                                                        0.42, 244.6, 39.52, 159.13, 30, 36.6)
    f4 = cal_flow_cw_threecylinder_threepressure_HPIPcombine_singleshaft(0.42, 276.5, 312.7, 51, 39.52, 30, 36.6, 92, 7)
    f5 = cal_flow_cw_threecylinder_threepressure_HPIPcombine_singleshaft(0.745, 353.7, 274.391, 53.274, 52.319, 33,
                                                                         43.8,
                                                                         92, 9.8)
    # print(format(f1, '.1f'))
    # print(format(f2, '.1f'))
    # print(format(f3, '.1f'))
    # print(format(f4, '.1f'))
    print(format(f5, '.1f'))

    # path = os.path.abspath('.') + '\\07_运行数据-202106-202112-联络门关闭.xlsx'
    # data_1 = pd.read_excel(path, 0)
    # data_2 = pd.read_excel(path, 1)
    # data_1 = float(data_1)
    #
    # row_table_1 = data_1.shape[0]  # 行数
    # col_table_1 = data_1.shape[1]  # 列数
    # row_table_2 = data_2.shape[0]  # 行数
    # col_table_2 = data_2.shape[1]  # 列数
    # print(data_1[8, 8])
