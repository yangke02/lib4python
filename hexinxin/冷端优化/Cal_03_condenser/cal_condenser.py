"""
程序功能：凝汽器热力性能计算
程序作者：何欣欣
程序生成日期：2023年8月2日
最近修改日期：
最近修改内容：
"""
import numpy as np

from Property_Steam import tw_P, rho_PT, p_T

# =================定义通用参数
cp_water = 4.186  # 水比热
pi = 3.14159  # 圆周率

FDU_pipe = 1.05  # 冷却管堵管率（/）
material_pipe = 6  # 冷却水管材类型 （1-钛管TI; 2-海军黄铜管HSN70-1; 3-白铜管B30; 4.白铜管B10; 5-铝黄铜HA117-1; 6-不锈钢304; 7-不锈钢316、317）
factor_design = 0.85  # 冷却水管清洁系数（/）
num_pass = 2  # 流程数（/）
od_pipe = 0.025  # 冷却水管外径（m）
thickness_pipe = 0.0005  # 冷却水管壁厚（m）
area_cond = 12600  # 凝汽器面积（m2）
num_pipe = 12364  # 冷却管总管数（/）


# FDU_pipe = 1  # 冷却管堵管率（/）
# material_pipe = 6  # 冷却水管材类型 （1-钛管TI; 2-海军黄铜管HSN70-1; 3-白铜管B30; 4.白铜管B10; 5-铝黄铜HA117-1; 6-不锈钢304; 7-不锈钢316、317）
# factor_design = 0.85  # 冷却水管清洁系数（/）
# num_pass = 2  # 流程数（/）
# od_pipe = 0.025  # 冷却水管外径（m）
# thickness_pipe = 0.0004553  # 冷却水管壁厚（m）
# area_cond = 35600  # 凝汽器面积（m2）
# num_pipe = 34467  # 冷却管总管数（/）


# ================已知凝汽器压力、进口水温、出口水温、冷却水流量，计算凝汽器参数
def cal_factor_cleanliness(p_condenser, t_in_condenser, t_out_condenser, flow_cw):
    num_pipe_real = num_pipe / FDU_pipe  # 实际有效管根数
    id_pipe = od_pipe - 2 * thickness_pipe  # 冷却管内径
    num_pipe_singlepass = num_pipe_real / num_pass  # 一个流程内的冷却水管根数
    area_pipe = pi * (id_pipe / 2) ** 2 * num_pipe_singlepass  # 一个流程内的冷却水管总面积
    v_pipe = flow_cw / area_pipe / 3600  # 管内平均流速
    ts = tw_P(p_condenser / 1000)  # 凝汽器压力对应的饱和蒸汽温度
    t_rise = t_out_condenser - t_in_condenser
    t_LMTD = t_rise / (np.log((ts - t_in_condenser) / (ts - t_out_condenser)))
    q_cond = flow_cw * cp_water * t_rise * 1000  # 凝汽器换热量,kJ/h

    Kt = q_cond / area_cond / t_LMTD / 3.6  # 凝汽器传热系数
    K0_HEI = cal_K0(od_pipe, v_pipe)  # HEI基本传热系数K0
    beta_t_HEI = cal_deta_t(t_in_condenser)  # HEI冷却水进口温度修正系数βt
    beta_m_HEI = cal_deta_m(material_pipe, thickness_pipe)  # HEI管材和壁厚修正系βm
    beta_c_HEI = factor_design
    K_HEI = K0_HEI * beta_t_HEI * beta_m_HEI * beta_c_HEI  # HEI总体传热系数KHEI
    factor_cleanliness = Kt * beta_c_HEI / K_HEI  # 清洁度系数
    t_d = t_rise / (np.exp(area_cond * K_HEI / cp_water / (flow_cw / 3.6 * rho_PT(1.5, t_in_condenser))) - 1.0)  # 凝汽器端差
    t_s = t_d + t_rise + t_in_condenser  # 凝汽器饱和温度
    p_cond = p_T(t_s) * 1000  # 凝汽器压力，kPa

    return factor_cleanliness, q_cond, p_cond


# ================计算HEI基本传热系数K0
def cal_K0(od=0.026, v=1.0):
    v = 3.2808 * v  # 由单位M/S转化为FT/SEC
    od = 39.37 * od  # 单位由米转化为英寸
    if 0.625 <= od <= 0.75:
        coef = -0.0013 * v ** 6 + 0.0637 * v ** 5 - 1.2514 * v ** 4 + 12.587 * v ** 3 - 71.025 * v ** 2 + 276.81 * v + 18.078
    elif 0.75 <= od <= 0.875:
        coef = -0.0015 * v ** 6 + 0.0736 * v ** 5 - 1.4391 * v ** 4 + 14.43 * v ** 3 - 80.759 * v ** 2 + 302.46 * v - 11.74
    elif 0.875 <= od <= 1.0:
        coef = -0.0015 * v ** 6 + 0.0736 * v ** 5 - 1.443 * v ** 4 + 14.51 * v ** 3 - 81.407 * v ** 2 + 304.44 * v - 17.453
    elif 1.0 <= od <= 1.125:
        coef = -0.0016 * v ** 6 + 0.0784 * v ** 5 - 1.5306 * v ** 4 + 15.292 * v ** 3 - 84.996 * v ** 2 + 311.75 * v - 25.407
    elif 1.125 <= od <= 1.25:
        coef = -0.0015 * v ** 6 + 0.0735 * v ** 5 - 1.4345 * v ** 4 + 14.312 * v ** 3 - 79.498 * v ** 2 + 295.38 * v - 9.2559
    elif 1.25 <= od <= 1.375:
        coef = -0.0015 * v ** 6 + 0.072 * v ** 5 - 1.4092 * v ** 4 + 14.091 * v ** 3 - 78.421 * v ** 2 + 292.1 * v - 8.2762
    elif 1.375 <= od <= 1.5:
        coef = -0.0014 * v ** 6 + 0.0705 * v ** 5 - 1.3839 * v ** 4 + 13.869 * v ** 3 - 77.343 * v ** 2 + 288.81 * v - 7.2965
    elif 1.5 <= od <= 1.625:
        coef = -0.0017 * v ** 6 + 0.0808 * v ** 5 - 1.5638 * v ** 4 + 15.548 * v ** 3 - 84.818 * v ** 2 + 306.11 * v - 26.079
    elif 1.625 <= od <= 1.75:
        coef = -0.0019 * v ** 6 + 0.0912 * v ** 5 - 1.7438 * v ** 4 + 17.046 * v ** 3 - 92.292 * v ** 2 + 323.41 * v - 44.862
    elif 1.75 <= od <= 1.875:
        coef = -0.0017 * v ** 6 + 0.0803 * v ** 5 - 1.5512 * v ** 4 + 15.299 * v ** 3 - 83.772 * v ** 2 + 301.76 * v - 26.021
    else:
        coef = -0.0014 * v ** 6 + 0.0695 * v ** 5 - 1.3586 * v ** 4 + 13.553 * v ** 3 - 75.252 * v ** 2 + 280.11 * v - 7.1812

    coef = coef / 3600 / 0.0929 * 1.8 * 1055.056  # 由单位BTU/(FT^2*℉*hr)转化为J/(M^2*℃*sec)
    return coef


# ================计算HEI冷却水进口温度修正系数βt
def cal_deta_t(t_in_condenser):
    tt = (t_in_condenser * 1.8 + 32) / 100
    coef = -1.122 * 10 * tt ** 6 + 4.785 * 10 * tt ** 5 - 7.978 * 10 * tt ** 4 + 6.564 * 10 * tt ** 3 - 2.826 * 10 * tt ** 2 + 0.07005 * 100 * tt - 0.1418
    return coef


# ================计算HEI管材和壁厚修正系βm
def cal_deta_m(index=6, thickness=0.0005):
    tt = thickness * 39.37  # 单位由米转化为英寸
    if index == 1:
        coef = -1707900 * tt ** 6 + 572627 * tt ** 5 - 74445 * tt ** 4 + 4723 * tt ** 3 - 138.33 * tt ** 2 - 2.8019 * tt + 1.035
    elif index == 2:
        coef = -980364 * tt ** 6 + 376599 * tt ** 5 - 57480 * tt ** 4 + 4428 * tt ** 3 - 180.25 * tt ** 2 + 2.5764 * tt + 1.022
    elif index == 6:
        coef = 168582 * tt ** 6 - 88093 * tt ** 5 + 17430 * tt ** 4 - 1768 * tt ** 3 + 118.85 * tt ** 2 - 9.4899 * tt + 1.064
    elif index == 7:
        coef = 19.985 * tt ** 2 - 6.7775 * tt + 1.0295
    else:
        coef = 1

    return coef


if __name__ == '__main__':
    # flow = flow_cw_from_pump(num_pump=3)  # 循环水流量
    # print(flow)
    # ttt = cal_factor_cleanliness(11.75, 35.9, 46.9, 55682.3)
    print('kkk')
    # t1 = cal_factor_cleanliness(9.28, 32.1, 41, flow)
    # t2 = cal_factor_cleanliness(10.00, 32.1, 41, flow)
    # t3 = cal_factor_cleanliness(8.70, 32.1, 41, flow)
    # t4 = cal_factor_cleanliness(8.11, 32.1, 41, flow)
    # print(t1)
    # print(t2)
    # print(t3)
    # print(t4)
