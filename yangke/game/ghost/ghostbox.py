import platform
from ctypes import *
import os
from yangke.common.config import logger

dll = windll.LoadLibrary(os.path.join(os.path.dirname(__file__), "gbild64.dll"))
dll.getmodel.restype = c_char_p
dll.getserialnumber.restype = c_char_p
dll.getproductiondate.restype = c_char_p
dll.getfirmwareversion.restype = c_char_p
dll.readstring.restype = c_char_p
dll.encryptstring.restype = c_char_p
dll.decryptstring.restype = c_char_p


def opendevice(index):
    return dll.opendevice(index)


def opendevicebyid(vid, pid):
    return dll.opendevicebyid(vid, pid)


def opendevicebypath(path):
    return dll.opendevicebypath(bytes(path, "utf-8"))


def isconnected():
    return dll.isconnected()


def closedevice():
    return dll.closedevice()


def resetdevice():
    return dll.resetdevice()


# �豸��Ϣ
def getmodel():
    return dll.getmodel().decode("utf-8")


def getserialnumber():
    return dll.getserialnumber().decode("utf-8")


def getproductiondate():
    return dll.getproductiondate().decode("utf-8")


def getfirmwareversion():
    return dll.getfirmwareversion().decode("utf-8")


# ���̲���
def presskeybyname(key):
    """
    按下键盘按键
    :param key: 键名，如"A", "1", "win", "ctrl"等，参见click_key
    """
    return dll.presskeybyname(bytes(key, "utf-8"))


def click_key(key: str):
    """
    点击键盘按键，可以是组合键，最多支持3个按键的组合键、如”ctrl+F", "ctrl+alt+F", "win+F"。可用键名包括：
    [0-9A-Z]
    F1-F12
    Num0-Num0
    NumLock、NumDot、NumEnter、NumAdd、NumDec、NumMul、NumDiv
    Ctrl、Alt、Shift、Win
    Up、Down、Left、Right
    RCtrl、RShift、RAlt、RGUI
    `-=[]\;',./
    Enter、Esc、BackSpace、Tab、CapsLock、PrintScreen、ScrollLock、Delete
    Pause、Insert、Home、End、PageUp、PageDown、

    :param key: 键位描述，如”ctrl+F", "ctrl+alt+F", "win+F"
    """
    if "+" in key:
        keys = key.split("+")
        if len(keys) == 2:
            presskeybyname(keys[0])
            pressandreleasekeybyname(keys[1])
            releasekeybyname(keys[0])
        elif len(keys) == 3:
            presskeybyname(keys[0])
            presskeybyname(keys[1])
            pressandreleasekeybyname(keys[2])
            releasekeybyname(keys[1])
            releasekeybyname(keys[0])
    else:
        pressandreleasekeybyname(key)


def presskeybyvalue(keyv):
    return dll.presskeybyvalue(keyv)


def releasekeybyname(key):
    """
    释放键盘按键
    :param key: 键名，如"A", "1", "win", "ctrl"等，参见click_key
    """
    return dll.releasekeybyname(bytes(key, "utf-8"))


def releasekeybyvalue(keyv):
    return dll.releasekeybyvalue(keyv)


def pressandreleasekeybyname(keyn):
    return dll.pressandreleasekeybyname(bytes(keyn, "utf-8"))


def pressandreleasekeybyvalue(keyv):
    return dll.pressandreleasekeybyvalue(keyv)


def iskeypressedbyname(keyn):
    return dll.iskeypressedbyname(bytes(keyn, "utf-8"))


def iskeypressedbyvalue(keyv):
    return dll.iskeypressedbyvalue(keyv)


def releaseallkey():
    return dll.releaseallkey()


def inputstring(str):
    return dll.inputstring(bytes(str, "utf-8"))


def getcapslock():
    return dll.getcapslock()


def getnumlock():
    return dll.getnumlock()


def setcasesensitive(cs):
    return dll.setcasesensitive(cs)


def setpresskeydelay(maxd, mind):
    return dll.setpresskeydelay(maxd, mind)


def setinputstringintervaltime(maxd, mind):
    return dll.setinputstringintervaltime(maxd, mind)


# ������
def pressmousebutton(mbtn):
    return dll.pressmousebutton(mbtn)


def releasemousebutton(mbtn):
    return dll.releasemousebutton(mbtn)


def pressandreleasemousebutton(mbtn):
    return dll.pressandreleasemousebutton(mbtn)


def ismousebuttonpressed(mbtn):
    return dll.ismousebuttonpressed(mbtn)


def releaseallmousebutton():
    return dll.releaseallmousebutton()


def movemouserelative(x, y):
    return dll.movemouserelative(x, y)


def movemouseto(x, y):
    try:
        return dll.movemouseto(x, y)
    except Exception as e:
        logger.warning(f"移动鼠标报错，移动的目标坐标不能和现有坐标相同")
        print(e)


def setmouseposition(x, y):
    return dll.setmouseposition(x, y)


def movemousewheel(z):
    return dll.movemousewheel(z)


def getmousex():
    return dll.getmousex()


def getmousey():
    return dll.getmousey()


def setmouseposition(x, y):
    return dll.setmouseposition(x, y)


def setpressmousebuttondelay(maxd, mind):
    return dll.setpressmousebuttondelay(maxd, mind)


def setmousemovementdelay(maxd, mind):
    return dll.setmousemovementdelay(maxd, mind)


def setmousemovementspeed(speedvalue):
    return dll.setmousemovementspeed(speedvalue)


def setmousemovementmode(modevalue):
    """
    设置鼠标移动方式，针对movemouseto命令，支持如下三种模式：
      1 直线移动（原移动方式，默认）
      2 曲线移动（贝塞尔曲线）
      3 极速移动（以最快速度移动到目标坐标）
    该功能无记忆，软件每次启动都要重新设置，否则将使用默认值
    """
    return dll.setmousemovementmode(modevalue)


def getproductionname():
    logger.debug("该接口有问题")
    return dll.getproductionname()


def initializedongle():
    return dll.initializedongle()


def setreadpassword(writepwd, newpwd):
    return dll.setreadpassword(bytes(writepwd, "utf-8"), bytes(newpwd, "utf-8"))


def setwritepassword(oldpwd, newpwd):
    return dll.setwritepassword(bytes(oldpwd, "utf-8"), bytes(newpwd, "utf-8"))


def readstring(readpwd, addr, count):
    return dll.readstring(bytes(readpwd, "utf-8"), addr, count).decode("utf-8")


def writestring(writepwd, str, addr):
    return dll.writestring(bytes(writepwd, "utf-8"), bytes(str, "utf-8"), addr)


def setcipher(writepwd, cipher):
    return dll.setcipher(bytes(writepwd, "utf-8"), bytes(cipher, "utf-8"))


def encryptstring(str):
    return dll.encryptstring(bytes(str, "utf-8")).decode("utf-8")


def decryptstring(str):
    return dll.decryptstring(bytes(str, "utf-8")).decode("utf-8")


def setclientscreenresolution(width, height):
    return dll.setclientscreenresolution(width, height)


def getclientscreenresolution():
    return dll.getclientscreenresolution()


if __name__ == "__main__":
    print(1)
