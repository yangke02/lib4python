import {fileURLToPath, URL} from 'node:url'
import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import tailwindcss from "tailwindcss"
//@ts-ignore
import postCssToRem from 'postcss-pxtorem'
import fs from 'fs'

// https://vitejs.dev/config/
export default defineConfig({
    base: '/',
    server: {
        host: '0.0.0.0',
        https: {
            key: fs.readFileSync('certs/localhost_ipv6_YangKeTSJ_key.pem'),
            cert: fs.readFileSync('certs/localhost_ipv6_YangKeTSJ.pem'),
        }
    },
    plugins: [
        vue(),
    ],
    resolve: {
        alias: {
            '@': fileURLToPath(new URL('./src', import.meta.url))
        }
    },
    css: {
        postcss: {
            plugins: [
                postCssToRem({
                    rootValue: 16, // 1rem的大小=16px
                    propList: ['*'], // 需要转换的属性，这里选择全部都进行转换
                }),
                tailwindcss({
                    content: ['./src/**/*.{vue,js,ts}'],
                })
            ]
        }
    },
    assetsInclude: ["public/**/*"]
})
