/// <reference types="vite/client" />
declare module "*.vue" {
    import type {DefineComponent} from "vue";
    const vueComponent: DefineComponent<{}, {}, any>
    export default vueComponent
}

declare interface Window {
    pywebview: {
        api: {
            start_debug: () => Promise<string>;
            stop_debug: () => Promise<string>;
            hook_sys_keyboard: (e: KeyboardEvent) => Promise<boolean>;
            shield_win_hotkeys: () => Promise<string>;
            keydown: (key: string) => Promise<string>;
            keyup: (key: string) => Promise<string>;
            mousedown: (x: number, y: number, buttons: number) => Promise<string>;
            mouseup: (x: number, y: number, buttons: number) => Promise<string>;
            mousemove: (x: number, y: number, buttons: number) => Promise<string>;
            release_all_keys: () => Promise<void>;
            dispatch_snapshot: (snapshot: string) => Promise<string>;
        }
    },  // 使得pywebview的接口不会报错
}

