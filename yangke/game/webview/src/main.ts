import {createApp} from 'vue'
import {createPinia} from 'pinia'
import {createYkUI} from 'yklib'
import App from './App.vue'
import router from './router'
import 'yklib/dist/style.css'
import "tailwindcss/tailwind.css"


const app = createApp(App)
app.use(createYkUI())
app.use(createPinia())
app.use(router)

app.mount('#app')
