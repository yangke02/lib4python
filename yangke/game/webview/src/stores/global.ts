import {ref, computed} from 'vue'
import {defineStore} from 'pinia'

export const useGlobalStore = defineStore('global', () => {
    const videoCardWidth = ref(1920)
    const videoCardHeight = ref(1080)

    const taskbarText = ref("")
    return {videoCardWidth, videoCardHeight, taskbarText}
})
