# -*- mode: python ; coding: utf-8 -*-
import sys
sys.setrecursionlimit(sys.getrecursionlimit()*5)


block_cipher = None


a = Analysis(
    ['gf_ui.py'],
    pathex=[r'C:\Users\54067\.conda\envs\python310\Lib\site-packages\paddleocr',
            r'C:\Users\54067\.conda\envs\python310\Lib\site-packages\paddle\libs'
            ],
    binaries=[],
    datas=[(r"C:\Users\54067\.conda\envs\python310\Lib\site-packages/pyecharts", "./pyecharts"),
           # (r"C:\Users\54067\.conda\envs\python310\Lib\site-packages\paddleocr\ppocr", "./ppocr"),
           (r"C:\Users\54067\.conda\envs\python310\Lib\site-packages\paddleocr", "./paddleocr"),
           (r"E:\lib4python\yangke\common\ui", "./ui"),
           (r"E:\lib4python\yangke\game\ui", "./ui"),
           ],
    hiddenimports=['paddleocr.tools',
                   'paddleocr.ppocr', 'paddleocr.ppstructure', 'shapely', 'extract_textpoint_slow',
                   'extract_textpoint_fast'
                   ],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False,
)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='gf_ui',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    upx_exclude=[],
    runtime_tmpdir=None,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
)
coll = COLLECT(
    exe,
    a.binaries,
    a.zipfiles,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='gf_ui',
)
