# from ultralytics import YOLO
#
# # Load a pre-trained YOLOv10n model
# model = YOLO("yolov10n.pt")
#
# # Perform object detection on an image
# results = model("bus.jpg")
#
# # Display the results
# results[0].show()
import os.path

# 训练yolo v10时
from ultralytics import YOLO

# Load YOLOv10n model from scratch
model_file = os.path.abspath("yolov10n.yaml")
# model = YOLO(model_file)
model = YOLO("runs/detect/train6/weights/best.pt")
# Train the model
# data = os.path.abspath("coco8.yaml")
# model.train(data=data, epochs=20, imgsz=[2517, 1346])

results = model("test.jpg")

results[0].show()
