import os.path
import typing

from yangke.common.config import logger
from yangke.ebsilon.graphicsview import YkGraphicsItem, Port
from yangke.ebsilon.constant.constant import Pressure, Temperature, Enthalpy, MassFlow, Power
from yangke.ebsilon.values import Values
from yangke.performance.iapws97 import get_h_by_pt
from yangke.common.QtImporter import QLineF, QPointF, Qt, QPoint, QPainter, QWidget, QStyleOptionGraphicsItem, QIcon

SpecificationValues = {
    "测点": [
        ["标签名", "<功率>", "", "", "tacit"],  # tacit表示该参数不接受计算结果的更新显示，也就是是作为计算结果，不会参与进一步的计算
        ["标签", "<DCS1.POWER>", "", "", "tacit"],
        ["值", "value", "###", Power("MW"), "tacit"],
    ],
}

Result = {

}


class Item(YkGraphicsItem):
    def __init__(self):
        super(Item, self).__init__()
        self.EBS_ID = 20001  # ebsilon软件中的组件id
        self.NAME = "输入参数"
        self.EBS_NAME = "从DCS或SIS采集的参数"  # ebsilon中的组件名
        self.EBS_TYPE = "Input Value"  # ebsilon帮助文件中组件所属的组
        # self.icon = QIcon(os.path.join(os.path.dirname(__file__), "Comp1.PNG"))  # 组件的图标

        # Ebsilon组件面板中的Ports页信息
        self.ports = {1: Port(idx=1, point=QPointF(0, 18), type1="Outlet",
                              description="System-boundary with value definition")}
        self.SpecificationValues = SpecificationValues
        self.Result = Result

    def paint(self, painter: QPainter, option: QStyleOptionGraphicsItem,
              widget: typing.Optional[QWidget] = ...) -> None:
        """
        绘制组件图形

        :param painter:
        :param option:
        :param widget:
        :return:
        """
        # 绘制组件实体
        self.pen[0].setColor(Qt.red)
        self.pen[0].setWidth(4)
        painter.setPen(self.pen[0])
        painter.drawLine(QLineF(QPointF(0, 0), QPointF(0, 16)))

        self.pen[1].setColor(Qt.black)
        self.pen[1].setWidth(1)
        painter.setPen(self.pen[1])  # 定义轮廓

        self.brush[0].setColor(Qt.yellow)
        painter.setBrush(self.brush[0])  # 定义填充属性
        painter.drawPolygon([QPoint(-20, -20), QPoint(20, -20), QPoint(0, 0)], Qt.OddEvenFill)  # 三角形

        super(Item, self).paint(painter, option, widget)  # 必须最后调用

    def calculate(self, step=None):
        """
        1号组件无需计算，该组件只是给出边界条件的值，只需将上一个迭代步的值赋给当前迭代步即可

        :return:
        """
        if step == 0:  # 如果迭代步是0，则说明是初始化过程，组件46的初始化过程参数值由图形界面赋值给self.values，与本方法无关
            ...
        else:
            values = self.values.values_steps.get(step - 1).copy()
            self.values.set_values(step=step, values=values)
        return "done"  # 该组件无需计算，直接返回done表示计算完成

    def spread_values(self, step=None, symbols=None):
        """
        将组件的参数传递给各个端口。
        例如在汽轮机组件中，进汽压力和排汽压力分别为P1和P2，但在对应的端口上，则只有P这一个参数，与端口连接的其他组件
        也只会查询端口上名为P的参数作为该点的压力。Comp1组件为边界条件设置组件，所有的参数名无需更改可以直接赋值给其唯一的一个连接端口。
        参数的传递会传递参数的位置，如设置的参数会传递给values.values，而不是传递给values.result。

        :param step: 迭代步
        :param symbols: 需要传递的参数
        """
        port = self.ports.get(1)
        if not isinstance(port.values, Values):
            logger.warning(f"{step}:{self.scene_id}号组件端口数据传递失败！")
        port.values.set_values(step=step, values=self.values, set_none=False)
