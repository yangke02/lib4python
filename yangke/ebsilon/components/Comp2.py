import os.path
import typing

from yangke.common.QtImporter import QRectF, QLineF, QPointF, Qt, QPoint, QPainter, QWidget, QStyleOptionGraphicsItem, \
    QColor, QImage, QIcon

from yangke.ebsilon.constant.constant import *
from yangke.ebsilon.graphicsview import YkGraphicsItem

SpecificationValues = {
    "流体状态参数(总，滞止)": [
        ["焓", "H", "", Enthalpy("kJ/kg")],
        ["质量流量", "M", "", MassFlow("t/h")],
    ],
    "轴和电气参数": [
        ["频率/转速(轴和输电线路)", "F", "", None, "disabled"],
        ["功率", "Q", "", Power("MW")],
    ],
}

Result = {
    "热参数": [
        ["0℃净热值(输入值)", "NCVI", "", Enthalpy("kJ/kg")],
    ]
}


class Item(YkGraphicsItem):
    def __init__(self):
        super(Item, self).__init__()
        self.EBS_ID = 2  # ebsilon软件中的组件id
        self.NAME = "阀门"
        self.EBS_NAME = "Throttle"  # ebsilon中的组件名
        self.EBS_TYPE = "Throttles and Valves"  # ebsilon帮助文件中组件所属的组
        self.icon = QIcon(os.path.join(os.path.dirname(__file__), "Comp2.PNG"))  # 组件的图标
        self.bounding = QRectF(-80 - self.pen_width / 2, -60 - self.pen_width / 2, 80 + self.pen_width / 2,
                               60 + self.pen_width / 2)

        self.SpecificationValues = SpecificationValues
        self.Result = Result

    def boundingRect(self) -> QRectF:
        """
        定义了组件item在视图场景中的范围，该范围会响应鼠标点击、碰撞检测等方法
        :return:
        """
        return self.bounding

    def paint(self, painter: QPainter, option: QStyleOptionGraphicsItem,
              widget: typing.Optional[QWidget] = ...) -> None:
        """
        绘制组件图形

        :param painter:
        :param option:
        :param widget:
        :return:
        """
        super(Item, self).paint(painter, option, widget)
        # 绘制组件实体
        if self.hasFocus():
            self.pen[0].setColor(QColor(255, 0, 0, 200))
        else:
            self.pen[0].setColor(QColor(100, 100, 100, 100))
        painter.setPen(self.pen[0])
        self.brush[0].setColor(self.brush_color)
        painter.setBrush(self.brush[0])
        # painter.drawImage(self.bounding, QImage(self.icon))
