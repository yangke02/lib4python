import random
import time
import os

"""
http://127.0.0.1:5000/?Action=gas&CH4=0.9&N2=0.001&CO2=0.002&C2H6=0.003&C3H8=0.002&H2O=0.001&H2S=0.0001&H2=0&CO=0&O2=0&C4H10=0&C4H10_2=0&C5H12=0&C5H12_2=0&C6H14=0&C7H16=0&C8H18=0&C9H20=0&C10H22=0&He=0&Ar=0&C2H4=0&C3H6=0&C4H8=0"
"""

import xlwings as xw


def gas_properties(compositions=None, p=101.325, t=15, t0=15):
    """
    计算天然气物性，p为天然气压力，t为天然气温度，t0为基准温度

    可选组分包括:
    1	 methane
    2	 ethane
    3	 propane
    4	 n‑butane
    5	 2‑methylpropane
    6	 n‑pentane
    7	 2‑methylbutane
    8	 2,2‑dimethylpropane
    9	 n‑hexane
    10	 2‑methylpentane
    11	 3‑methylpentane
    12	 2,2‑dimethylbutane
    13	 2,3‑dimethylbutane
    14	 n‑heptane              C7H16       庚烷
    15	 n‑octane               C8H18       辛烷
    16	 n‑nonane               C9H20       壬烷
    17	 n‑decane               C10H22      癸烷
    18	 ethene                 C2H4        乙烯
    19	 propene
    20	 1‑butene
    21	 cis‑2‑butene
    22	 trans‑2‑butene
    23	 2‑methylpropene
    24	 1‑pentene
    25	 propadiene
    26	 1,2‑butadiene
    27	 1,3‑butadiene
    28	 ethyne
    29	 cyclopentane
    30	 methylcyclopentane
    31	 ethylcyclopentane
    32	 cyclohexane
    33	 methylcyclohexane
    34	 ethylcyclohexane
    35	 benzene
    36	 toluene
    37	 ethylbenzene
    38	 o‑xylene
    39	 methanol
    40	 methanethiol
    41	 hydrogen
    42	 water
    43	 hydrogen sulfide
    44	 ammonia
    45	 hydrogen cyanide
    46	 carbon monoxide
    47	 carbonyl sulfide
    48	 carbon disulfide
    49	 helium
    50	 neon
    51	 argon
    52	 nitrogen
    53	 oxygen
    54	 carbon dioxide
    55	 sulfur dioxide
    56	 n-undecane
    57	 n-dodecane
    58	 n-tridecane
    59	 n-tetradecane
    60	 n-pentadecane
    """
    if compositions is None:
        compositions = {"CH4": 0.99, "N2": 0.01}
    comp_id = {
        "CH4": 1,
        "C2H6": 2,
        "C3H8": 3,
        "C4H10": 4,  # 正丁烷
        "C4H10-2": 5,  # 异丁烷
        "C5H12": 6,  # 正戊烷
        "C5H12-2": 7,  # 异戊烷
        "C5H12-3": 8,  # 新戊烷，二甲基丙烷
        "C6H14": 9,  # 己烷
        "C6H14-1": 10,  # 2-甲基戊烷
        "C6H14-2": 11,  # 3-甲基戊烷
        "C6H14-3": 12,  # 2,2‑dimethylbutane
        "C6H14-4": 13,  # 2,3‑dimethylbutane
        "C7H16": 14,  # 庚烷
        "C8H18": 15,  # 辛烷
        "C9H20": 16,  # 壬烷
        "C10H22": 17,  # 癸烷
        "C2H4": 18,  # 乙烯
        "C3H6": 19,  # 丙烯
        "C4H8": 20,  # 1-丁烯
        "C3H4": 25,  # 丙二烯
        "C2H2": 28,  # 乙炔
        "H2": 41,
        "H2O": 42,
        "H2S": 43,  # 硫化氢
        "NH3": 44,
        "CO": 46,
        "He": 49,
        "Ar": 51,
        "N2": 52,
        "O2": 53,
        "CO2": 54,
        "SO2": 55,
    }
    app = xw.App(visible=True, add_book=False)
    app.display_alerts = False
    app.screen_updating = True
    root_p = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
    file_path = os.path.join(root_p, "sis", "resource", "ISO6976.xlsx")
    wb = app.books.open(file_path)
    sht1 = wb.sheets["Summary"]
    for i in range(9, 69, 1):  # 首先清空上次的计算结果
        sht1.range(f"A{i}").value = 0
    row = 9
    for k, v in compositions.items():
        sht1.range(f"A{row}").value = comp_id.get(k)
        sht1.range(f"C{row}").value = float(v)
        row += 1

    sht1.range("C2").value = t0
    sht1.range("C3").value = t0


gas_properties()
