import xlwings as xw


def main():
    wb = xw.Book.caller()
    sheet = wb.sheets[0]
    if sheet["A1"].value == "Hello xlwings!":
        sheet["A1"].value = "Bye xlwings!"
    else:
        sheet["A1"].value = "Hello xlwings!"


@xw.func
def hello(name):
    print("yangke")
    return f"Hello {name}11!"


if __name__ == "__main__":
    xw.Book("xlwings_macro.xlsm").set_mock_caller()
    main()
