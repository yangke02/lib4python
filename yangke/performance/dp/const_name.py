suffix_p_01: "去标题行" = "_p预处理_修正"
# noinspection PyUnresolvedReferences
suffix_p_10: "剪切时间" = "_剪切p时间_修正"
# noinspection PyUnresolvedReferences
suffix_p_20: "汇总" = "p汇总_修正"
# noinspection PyUnresolvedReferences
suffix_dcs_01 = "_dcs预处理_修正"
# noinspection PyUnresolvedReferences
suffix_dcs_10: "剪切时间" = "_剪切dcs时间_修正"
# noinspection PyUnresolvedReferences
suffix_dcs_20: "汇总" = "dcs汇总_修正"
# noinspection PyUnresolvedReferences
suffix_imp_10: "剪切时间" = "_剪切imp时间_修正"
# noinspection PyUnresolvedReferences
suffix_imp_11 = "_大气压修正"
# noinspection PyUnresolvedReferences
suffix_imp_20: "汇总" = "imp汇总_修正"
