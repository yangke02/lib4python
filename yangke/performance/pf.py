def get_f_well_by_f_condense(f_condense):
    """
    根据凝结水流量计算热井水流量

    :param f_condense: 凝结水流量
    :return:
    """
    return 0.7583 * f_condense + 32.22

# def get_heat_load_by_
