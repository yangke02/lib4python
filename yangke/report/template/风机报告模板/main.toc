\ttl@change@i {\@ne }{chapter}{3.8em}{\hspace {-3.8em}}{\thecontentslabel ~~}{}{\titlerule *[4pt]{.}\contentspage }\relax 
\ttl@change@v {chapter}{}{}{}\relax 
\ttl@change@i {\@ne }{section}{38pt}{}{\contentslabel {22pt}}{\hspace *{-22pt}}{\titlerule *[0.3pc]{.}\contentspage }\relax 
\ttl@change@v {section}{}{}{}\relax 
\ttl@change@i {\@ne }{subsection}{70pt}{}{\contentslabel {32pt}}{\hspace *{-32pt}}{\titlerule *[0.3pc]{.}\contentspage }\relax 
\ttl@change@v {subsection}{}{}{}\relax 
\contentsline {chapter}{\fontsize {12pt}{12pt}\selectfont 摘\nobreakspace {}\nobreakspace {}\nobreakspace {}\nobreakspace {}要}{I}{section*.2}%
\contentsline {chapter}{\numberline {1\hspace {.3em}}灵活性改造的概念}{1}{section*.6}%
\contentsline {chapter}{\numberline {2\hspace {.3em}}灵活性改造解决的问题}{2}{section*.8}%
\contentsline {section}{\numberline {2.1}直接解决的问题}{2}{section*.10}%
\contentsline {section}{\numberline {2.2}间接解决的问题}{2}{section*.12}%
\contentsline {chapter}{\numberline {3\hspace {.3em}}提高火电机组灵活性的方法}{3}{section*.14}%
\contentsline {section}{\numberline {3.1}提高锅炉灵活性}{3}{section*.16}%
\contentsline {section}{\numberline {3.2}提高汽机灵活性}{3}{section*.19}%
\contentsline {section}{\numberline {3.3}建立储能系统}{3}{section*.20}%
\contentsline {section}{\numberline {3.4}燃气蒸汽联合循环}{3}{section*.21}%
\contentsfinish 
